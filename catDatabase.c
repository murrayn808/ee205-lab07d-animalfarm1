#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"
#define MAX_CATS 1024
#define MAX_NAME_LENGTH 50
//five arrays index represents what cat it is

//enum catgender{UNKNOWN_GENDER,MALE,FEMALE} testOne = MALE;
//enum catbreed{UNKNOWN_BREED,MAINE_COON,MANX,SHORTHAIR,PERSIAN,SPHYNX} testTwo = MANX;

/*struct Cat {
   enum catgender gender;
   enum catbreed breed;
   enum Color collarColor1;
   enum Color collarColor2;
   unsigned long long license;
   bool isFixed;
   float weight;
   char name[MAX_NAME_LENGTH];
   };
*/
struct Cat catArray[MAX_CATS]; 

char* colorName (enum Color color){
   switch (color) {
      case BLACK: return "Black";
      case WHITE: return "White";
      case RED:   return "Red";
      case BLUE:  return "Blue";
      case GREEN: return "Green";
      case PINK:  return "Pink";
   }

   return NULL;
};
/*enum catgender genders [MAX_CATS];
enum catbreed breeds [MAX_CATS];
enum Color collarColors1[MAX_CATS];
enum Color collarColors2[MAX_CATS];
unsigned long long licenses[MAX_CATS];

bool isFixed [MAX_CATS];//assume cat isnt fixed unless otherwise known
float weights [MAX_CATS];//cant be negative
char names [MAX_CATS][MAX_NAME_LENGTH];
*/

int numberOfCats = 0;
int initializeDatabase(){
   numberOfCats = 0;
   memset(&catArray[0],0,sizeof(catArray));
   return 0;
}



