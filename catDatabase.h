#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#pragma once
#define MAX_CATS 1024
#define MAX_NAME_LENGTH 50

enum catgender{UNKNOWN_GENDER,MALE,FEMALE};
enum catbreed{UNKNOWN_BREED,MAINE_COON,MANX,SHORTHAIR,PERSIAN,SPHYNX};
enum Color{BLACK,WHITE,RED,BLUE,GREEN,PINK};
char* colorName (enum Color color);
struct Cat {
   enum catgender gender;
   enum catbreed breed;
   enum Color collarColor1;
   enum Color collarColor2;
   unsigned long long license;
   bool isFixed;
   float weight;
   char name[MAX_NAME_LENGTH];
   };

extern char* colorName (const enum Color color);

extern struct Cat catArray[MAX_CATS];
/*extern enum Color collarColors1[MAX_CATS];
extern enum Color collarColors2[MAX_CATS];
//extern catgender testOne;
extern enum catgender genders[MAX_CATS];
//extern catbreed testTwo;
extern enum catbreed breeds[MAX_CATS];
extern bool isFixed[MAX_CATS];
extern float weights[MAX_CATS];
extern unsigned long long licenses[MAX_CATS];
extern char names[MAX_CATS][MAX_NAME_LENGTH];
*/
extern int initializeDatabase();
extern int numberOfCats; 
