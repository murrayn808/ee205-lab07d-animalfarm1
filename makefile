CC = gcc
CFLAGS = -g -Wall -Wextra
TARGET = animalfarm1
all: $(TARGET)

addCats.o: addCats.c catDatabase.h config.h
	$(CC) $(CFLAGS) -c addCats.c

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

deleteCats.o: deleteCats.c catDatabase.h
	$(CC) $(CFLAGS) -c deleteCats.c

reportCats.o: reportCats.c catDatabase.h config.h
	$(CC) $(CFLAGS) -c reportCats.c

UpdateCats.o: UpdateCats.c catDatabase.h addCats.h config.h
	$(CC) $(CFLAGS) -c UpdateCats.c

animalfarm1.o: main.c catDatabase.h addCats.h UpdateCats.h reportCats.h deleteCats.h config.h
	$(CC) $(CFLAGS) -c main.c
animalfarm1: addCats.o catDatabase.o deleteCats.o main.o reportCats.o UpdateCats.o
	$(CC) $(CFLAGS) -o $(TARGET) addCats.o catDatabase.o deleteCats.o main.o reportCats.o UpdateCats.o
clean:
	rm -f $(TARGET) *.o

